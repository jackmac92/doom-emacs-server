FROM silex/emacs:master-dev
RUN apt-get update && apt-get install -y make libssl-dev libghc-zlib-dev libcurl4-gnutls-dev libexpat1-dev gettext unzip socat fd-find
RUN wget https://github.com/git/git/archive/v2.23.0.tar.gz -O git.tar.gz && tar -xf git.tar.gz && cd git-* && make prefix=/usr/local all install
RUN curl -LO https://github.com/BurntSushi/ripgrep/releases/download/12.1.1/ripgrep_12.1.1_amd64.deb
RUN dpkg -i ripgrep_12.1.1_amd64.deb

RUN useradd -ms /bin/bash doom
RUN chown -R doom /home/doom

USER doom
WORKDIR /home/doom
RUN git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d
RUN yes | ~/.emacs.d/bin/doom install
RUN yes | ~/.emacs.d/bin/doom sync
RUN echo >> ~/.doom.d/init.el
RUN yes | ~/.emacs.d/bin/doom sync

COPY init.sh /home/doom/init-docker-emacs-server
USER root
RUN curl -fsSL https://deno.land/x/install/install.sh | sh && mv ~/.deno/bin/deno /usr/local/bin/deno && chmod 777 /usr/local/bin/deno
RUN chmod a+x /home/doom/init-docker-emacs-server
USER doom
COPY mod.ts /home/doom/mod.ts
RUN deno cache --unstable mod.ts

CMD /home/doom/init-docker-emacs-server
