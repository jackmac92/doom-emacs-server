#!/usr/bin/env bash
set -euo pipefail

emacs --bg-daemon

echo "Starting deno server"

deno run -A --unstable /home/doom/mod.ts
