const server = Deno.listen({ port: 8080, hostname: '0.0.0.0' });
console.log(`HTTP webserver running.  Access it at:  http://localhost:8080/`);

const passToEmacs = async (cmd: string) => {
  const process = Deno.run({
    cmd: ["/nix/store/emacs/bin/emacsclient", "-e", cmd],
    stdout: "piped",
    stderr: "piped",
  });
  const output = await process.output(); // "piped" must be set
  const outStr = new TextDecoder().decode(output);

  const error = await process.stderrOutput();
  const errorStr = new TextDecoder().decode(error);
    if (errorStr.length > 1) {throw new Error("Error detected: " + errorStr)}

  process.close();
  return outStr;
};

for await (const conn of server) {
  (async () => {
    const httpConn = Deno.serveHttp(conn);
    for await (const requestEvent of httpConn) {
      const url = new URL(requestEvent.request.url);
      const search = new URLSearchParams(url.search);
      const cmd = search.get("cmd");
      if (cmd === null) {
        requestEvent.respondWith(
          new Response("needs 'cmd'", {
            status: 400,
          })
        );
        continue;
      }
      const body = await passToEmacs(cmd);

      requestEvent.respondWith(
        new Response(body, {
          status: 200,
        })
      );
    }
  })();
}
